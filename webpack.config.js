const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    entry: './src/client/index.js',
    plugins: [
        new CleanWebpackPlugin(['public/build'])
    ],
    output: {
        filename: 'public/build/client.js',
        sourcePrefix: ''
    },
    node: {
        fs: 'empty'
    },
    devtool: 'source-map'
}