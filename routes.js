'use strict'

const express = require('express');
const router = express.Router();
const passport = require('passport');
const User = require('./src/server/models/User');
const UserProfile = require('./src/server/models/user-profile');
const config = require('./src/server/config/dbconfig');
const jwt = require('jsonwebtoken');
const logger = require('winston');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

router.use(express.static(__dirname + '/public'));

router.get('/', checkAuth, function(req, res) {
    logger.info(req.cookies.authToken);
    res.sendFile(__dirname + '/public/app.html');
});

router.get('/login', function(req, res) {
    res.sendFile(__dirname + '/public/login.html');
});

router.get('/signup', function(req, res) {
    res.sendFile(__dirname + '/public/signup.html');
});

router.post('/signup', (req, res, next) => {
    User.find({ email: req.body.email }).exec().then(user => {
        if (user.length >= 1) {
            return res.status(409).json({message: 'User Exist'});
        }
        else {
            bcrypt.hash(req.body.password, 10, (err, hash) => {
                if (err) {
                    return res.status(500).json({
                        error: err
                    });
                    res.redirect('/signup');
                }
                else {
                    const user = new User({
                        _id: mongoose.Types.ObjectId(),
                        name: req.body.name,
                        email: req.body.email,
                        password: hash
                    });
                    user.save().then(result => {
                        logger.info(user._id);
                        let userProfile = new UserProfile({
                            _id: user._id,
                            name: req.body.name,
                            email: user.email
                        });
                        userProfile.save().then(up => {
                            res.redirect('/login');
                        }).catch(err => {
                            logger.error(err);
                            res.status(500).json({
                                error: err
                            });
                        });
                    })
                    .catch(err => {
                        logger.error(err);
                        res.status(500).json({
                            error: err
                        });
                    });
                }
            });
        }
    });
});

router.post('/login', (req, res, next) => {
    User.findOne({ email: req.body.email }).exec().then(user => {
        logger.info('user '+user._id+' logging in');
        if (user.length < 1) {
            return res.status(401).json({
                mesage: 'Auth failed 1'
            });
        }
        let passwordMatch = bcrypt.compareSync(req.body.password, user.password);
        if(passwordMatch) {
            let token = jwt.sign({
                email: user.email,
                userId: user._id
            }, config.secret, {
                expiresIn: '1h'
            });
            res.cookie('authToken', token)
            res.redirect('/');
        } else {
            res.status(401).json({
                mesage: 'Auth failed 2'
            });
        }
    }).catch(err => {
        logger.error(err);
        res.status(500).json({
            error: err
        });
    });
});

router.delete('/:userId', (req, res, next) => {
    User.remove({ _id: req.params.userId }).exec().then(result => {
        return result.status(200).json({
            message: 'User deleted'
        });
    }).catch(err => {
        logger.error(err);
        res.status(500).json({
            error: err
        });
    });
});

router.get('/logout', function(req, res) {
    req.logout();
    res.clearCookie('authToken');
    res.redirect('/login');
});

function checkAuth(req, res, next) {
    try {
        const token = req.cookies.authToken;
        let decodedToken = jwt.verify(token, config.secret);
        req.userData = decodedToken;
        next();
    } catch (error) {
        res.redirect('/login');
    }
}

module.exports = router;
