'use strict';

// In production, add websocket connection as an environment variable
import displayTransitioner from './display-transitioner';
import $ from "jquery";
import io from 'socket.io-client';

// All displays
import Dashboard from './displays/dashboard';
import Projects from './displays/projects';
import RepairEstimator from './displays/repair-estimator';
import RehabEstimator from './displays/rehab-manager';
import CurrentProject from './displays/current-project';

let socket = io();

socket.on('connect', function() {
    initializeDisplays(socket);
});

/**
 * 
 * @param {socket} socket Websocket Connection
 */
function initializeDisplays(socket) {
    // Adding the displays so they can receive websocket events
    // Websockets enable us to view data in realtime for the entire app.
    displayTransitioner.init();
    displayTransitioner.add(new Dashboard(socket));
    displayTransitioner.add(new Projects(socket));
    displayTransitioner.add(new RepairEstimator(socket));
    displayTransitioner.add(new RehabEstimator(socket));
    displayTransitioner.add(new CurrentProject(socket));
    // displayTransitioner.add(new NewProject(socket));
    displayTransitioner.transition('dashboard', 0, 0);
}
