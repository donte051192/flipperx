'use strict'

import EventEmitter from 'events';
import $ from 'jquery';

class Display extends EventEmitter {
    /**
     * Create a display
     * @param {(HTMLElement|String)} display The element containing id the display
     */
    constructor(display) {
        super();

        let element = display;
        if(typeof display === 'string') {
            element = document.getElementById(display);
        }

        element.style.display = 'none';
        this._element = element;
    }

    /**
     * @type {String}
     * @readonly
     */
    get element() {
        return this._element;
    }

    /**
     * @type {String}
     * @readonly
     */
    get name() {
        return this._element.id;
    }

    /**
     * @returns {Promise}
     */
    open() {
        return Promise.resolve();
    }

    /**
     * @returns {Promise}
     */
    close() {
        return Promise.resolve();
    }

    transition(duration) {
        if (isNaN(duration)) {
            duration = 300;
        }
        return this._element.animate([
            {transform: 'translate3d(0, 50%, 10px)'},
            {transform: 'translate3d(0, 0, 0)'}
        ], {
            easing: 'cubic-bezier(.22, .67, .52, .92)',
            duration: duration
        }).finished;
    }
}

export default Display;
