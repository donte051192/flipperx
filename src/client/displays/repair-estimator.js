'use strict';

import $ from "jquery";
import Display from '../Display';
import displayTransitioner from '../display-transitioner';
import Mustache from 'mustache';

let catergories = ['Exterior', 'Walls', 'Flooring', 'Kitchen', 'Bathrooms', 'Plumbing', 'HVAC', 'Electrical', 'Doors', 'Staging', 'Demolition', 'Other', 'TOTAL'];

class RepairEstimator extends Display {
    constructor(ws) {
        super('repair-estimator');
        this.ws = ws;
        // this.socket = socket;
        this.displayname = 'repair-estimator';

        this.init();
    }

    init() {
        console.log('Initiating Repair Estimator');
        this.populateEstimator();
    }

    populateEstimator() {
        catergories.forEach(c => {
            let collapsible;
            if (c === 'TOTAL') {
                collapsible = [
                    `<div class="col-md-4 col-xlg-4">
                        <div class="card" style="border-radius: 5px">
                            <div class="card-header" id="heading${c}" style="background: #424242;"data-toggle="collapse" data-target="#collapse${c}" aria-expanded="true" aria-controls="collapse${c}">
                                <button class="pull-right btn btn-link rotate"><i class="fa fa-chevron-up" style="color: white;"></i></button>
                                    <button class="btn btn-link pull-left" style="border-radius: 5px; color: white;">
                                        ${c}
                                    </button>
                                    <h5 class="pull-right" style="line-height: initial; margin: 4px; color: #00C853;">
                                        $4,055
                                    </h5>
                                </div>
                            </div>
                            <div id="collapse${c}" class="collapse" aria-labelledby="heading${c}" data-parent="#accordion">
                                <div class="card-body" style="padding: 20px;">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>
                    </div>`
                ].join("\n");
            }
            else {
                collapsible = [
                    `<div class="col-md-4 col-xlg-4">
                        <div class="card" style="border-radius: 5px;">
                            <div class="card-header" id="heading${c}" data-toggle="collapse" data-target="#collapse${c}" aria-expanded="true" aria-controls="collapse${c}">
                                <div class="">
                                <button class="pull-right btn btn-link rotate"><i class="fa fa-chevron-up" style="color: black;"></i></button>
                                    <button class="btn btn-link pull-left" style="border-radius: 5px; color: black;">
                                        ${c}
                                    </button>
                                    <h5 class="pull-right" style="line-height: initial; margin: 4px;">
                                        $4,055
                                    </h5>
                                </div>
                            </div>
                            <div id="collapse${c}" class="collapse" aria-labelledby="heading${c}" data-parent="#accordion">
                                <div class="card-body" style="padding: 20px;">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>
                    </div>`
                ].join("\n");
            }

            let html = Mustache.render(collapsible, c);
            $("#categories").append(html);

            $(".rotate").click(function() {
                $(this).toggleClass("down")  ; 
            });
        });
    }

    open() {
        this.isopen = true;
        return Promise.resolve();
    }

    close() {
        this.isopen = false;
        return Promise.resolve();
    }
}

export default RepairEstimator;
