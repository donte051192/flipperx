'use strict';

import $ from "jquery";
import Display from '../Display';
import displayTransitioner from '../display-transitioner';
import Mustache from 'mustache';

class RehabManager extends Display {
    constructor(ws) {
        super('rehab-manager');
        this.ws = ws;
        // this.socket = socket;
        this.displayname = 'rehab-manager';

        this.init();
    }

    init() {
        console.log('Initiating New Project');
    }

    open() {
        this.isopen = true;
        return Promise.resolve();
    }

    close() {
        this.isopen = false;
        return Promise.resolve();
    }
}

export default RehabManager;
