'use strict';

import Display from '../Display';
import displayTransitioner from '../display-transitioner';
import Mustache from 'mustache';
import uuidv4 from 'uuid/v4';

let currentProject; // Key that contains the open project

class Projects extends Display {
    constructor(ws) {
        super('projects');
        this.ws = ws;
        // this.socket = socket;
        this.displayname = 'projects';
        this.allProjects = new Map();

        this.ws.on('newProjectAdded', data => {
            clearInputs();
            showConfirmation();
            this.populateProject([data]);
        });

        this.ws.on('allProjects', data => {
            this.initProjectMap(data);
            this.populateProject(data);
        });

        this.init();
    }

    init() {
        console.log('Getting All Projects');
        this.getAllProjects();
        this.setuplisteners();
    }

    setuplisteners() {
        let saveProjectBtn = document.getElementById('saveProject');
        saveProjectBtn.addEventListener('click', () => {
            let allInputs = document.querySelectorAll('[id^="newProject_"]');
            let validationMessage = document.getElementById('validationMessage');
            let propertyDetailsMap = new Map();
            let readyToSend = false;
            let uuid = uuidv4();

            console.log(allInputs);
            allInputs.forEach(input => {
                if (!input.checkValidity()) {
                    validationMessage.innerHTML = 'Please fill out '+input.dataset.name;
                    input.style.borderBottom = '2px solid #d9534f';
                    validationMessage.style.display = '';
                    readyToSend = false;
                }
                else {
                    input.style.border = '';
                    validationMessage.style.display = 'none';
                    propertyDetailsMap.set(input.dataset.name, input.value);
                    readyToSend = true;
                }
            });

            if (readyToSend) {
                let propertyDetails = {
                    id: uuid,
                    propertyName: propertyDetailsMap.get('Property Name'),
                    propertyType: propertyDetailsMap.get('Property Type'),
                    streetAddress: propertyDetailsMap.get('Street Address'),
                    city: propertyDetailsMap.get('City'),
                    state: propertyDetailsMap.get('State'),
                    zip: propertyDetailsMap.get('Zip Code'),
                    schoolDistrict: propertyDetailsMap.get('School District'),
                    beds: propertyDetailsMap.get('Bedrooms'),
                    baths: propertyDetailsMap.get('Bathrooms'),
                    squarefeet: propertyDetailsMap.get('Square Feet'),
                    mlsId: propertyDetailsMap.get('MLS Number')
                }
                this.addProject(propertyDetails);
            }
        });

        $("#newproject-modal").on("hidden.bs.modal", function () {
            console.log('closing modal');
            clearInputs();
        });
    }

    /**
     * 
     * @param {Array} data 
     */
    initProjectMap(data) {
        data.forEach(d => {
            this.allProjects.set(d.id, d);
        });
    }

    getAllProjects() {
        this.ws.emit('getAllProjects');
    }

    /**
     * 
     * @param {Array} data 
     */
    populateProject(data) {
        console.log('populating project');

        data.forEach(d => {
            let propertyCard = [
                `<div class="col-md-6 col-lg-6 col-xlg-4">
                    <div class="card card-block">
                        <div class="row">
                            <div class="col-md-4 col-lg-3 text-center" style="display:  none;">
                                <a href="app-contact-detail.html"><img src="../assets/images/users/1.jpg" alt="user" class="img-circle img-responsive"></a>
                            </div>
                            <div class="col-12">
                                <h3 class="box-title m-b-0" style="color: #2f3d4a;">${d.propertyName}</h3>
                                <small>${d.propertyType}</small>
                                <address>
                                    ${d.streetAddress}, ${d.city}, ${d.state} ${d.zip}
                                </address>
                                <div style="inline-flex;">
                                    <small>Bed: <b style="color: #0CC27E;">${d.beds}</b></small>
                                    <small>Bath: <b style="color: #0CC27E;">${d.baths}</b></small>
                                    <small>SqFt: <b style="color: #0CC27E;">${d.squarefeet}</b></small>
                                <div>
                            </div>
                            <button type="button" class="btn btn-secondary btn-circle" style="position: absolute; right: 0px; top: 0; margin-right: 15px; background: #EEEEEE;"><i class="mdi mdi-settings"></i> </button>
                            <button id="openProject|${d.id}" type="button" class="btn waves-effect waves-light btn-secondary btn-rounded text-white" data-toggle="modal" data-target="#openproject-modal" style="position: absolute; right: 0px; bottom: 0; margin-right: 15px; background: linear-gradient(to right, #0CC27E 0%, #2f3d4a 100%);">Open</button>
                        </div>
                    </div>
                </div>`
            ].join("\n");
            var html = Mustache.render(propertyCard, d);
            $("#projectRow").append(html);

            let openProjectBtn = document.getElementById(`openProject|${d.id}`);
            openProjectBtn.onclick = () => {
                let elementId = openProjectBtn.id.split('|')
                let projectId = elementId[1];
                let yesConfirmationOpenProject = document.getElementById('yesConfirmationOpenProject');
                let projectInfo = this.allProjects.get(projectId);
                let propertyName = projectInfo.propertyName;
                let openProjectName = document.getElementById('openProjectName');
                openProjectName.innerHTML = propertyName;
                yesConfirmationOpenProject.onclick = () => {
                    $('#openproject-modal').modal('toggle');
                    this.viewProject(projectId);
                };
            };
        });
    }

    /**
     * 
     * @param {UUID} projectId
     */
    viewProject(projectId) {
        let projectInfo = this.allProjects.get(projectId);
        let preloader = $(".preloader");

        preloader.fadeIn();
        setTimeout(function() {
            displayTransitioner.transition('current-project', 0, 0);
            preloader.fadeOut();
        }, 1200);

        currentProject = projectId;
        this.ws.emit('currentProject', currentProject);
        console.log(currentProject);
        console.log(projectInfo);
    }

    /**
     * 
     * @param {Object} obj property details obj
     */
    addProject(obj) {
        this.ws.emit('AddProject', obj);
    }

    open() {
        this.isopen = true;
        return Promise.resolve();
    }

    close() {
        this.isopen = false;
        return Promise.resolve();
    }
}

function clearInputs() {
    let allInputs = document.querySelectorAll('[id^="newProject_"]');
    let validationMessage = document.getElementById('validationMessage');
    allInputs.forEach(input => {
        input.value = '';
        input.style.border = '';
    });
    validationMessage.style.display = 'none';
}

function showConfirmation() {
    swal("Project Saved", "Go to projects to open", "success");
}

export default Projects;
