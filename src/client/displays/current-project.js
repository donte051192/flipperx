'use strict';

import Display from '../Display';
import Mustache from 'mustache';

class CurrentProject extends Display {
    constructor(ws) {
        super('current-project');
        this.ws = ws;
        // this.socket = socket;
        this.displayname = 'current-project';
        this.currentProject;
        ws.on('currentProject', projectId => {
            this.setCurrentProject(projectId);
        });
        
        this.setupListeners();
    }

    setupListeners() {
        let addImagesBtn = document.getElementById('addImages');
        let viewReportsBtn = document.getElementById('viewReports');
        let contractorDocumentsBtn = document.getElementById('contractorDocuments');
        let fundingPacketBtn = document.getElementById('fundingPacket');
        let VendorsBtn = document.getElementById('Vendors');
        let PromoteProjectBtn = document.getElementById('PromoteProject');
        let SellOrRentProjectBtn = document.getElementById('SellOrRentProject');

        addImagesBtn.onclick = () => {
            this.clearActionViewer();
            this.populateActionViewer('images');
        };
        viewReportsBtn.onclick = () => {
            this.clearActionViewer();
            this.populateActionViewer('reports');
        };
        contractorDocumentsBtn.onclick = () => {
            this.clearActionViewer();
            this.populateActionViewer('contractor-documents');
        };
        fundingPacketBtn.onclick = () => {
            this.clearActionViewer();
            this.populateActionViewer('funding-packet');
        };
        VendorsBtn.onclick = () => {
            this.clearActionViewer();
            this.populateActionViewer('vendors');
        };
        PromoteProjectBtn.onclick = () => {
            this.clearActionViewer();
            // this.populateActionViewer('');
        }; 
        SellOrRentProjectBtn.onclick = () => {
            this.clearActionViewer();
            // this.populateActionViewer('');
        };
    }

    setCurrentProject(projectId) {
        this.currentProject = projectId;
    }

    clearActionViewer() {
        let projectActionViewer = document.getElementById('projectActionViewer');
        while (projectActionViewer.hasChildNodes()) {
            projectActionViewer.removeChild(projectActionViewer.lastChild);
        }
    }

    populateActionViewer(action) {
        if (action === 'images') {
            // TODO Get Project Images
            let count = 17;

            for (let i = 0; i < count; i++) {
                let image = [
                    `<div class="col-md-4">
                        <div class="card section-box-eleven">
                            <!-- <figure>
                                <img class="card-img img-fluid" src="../assets/images/big/img4.jpg" alt="Card image">
                            </figure> -->
                            <figure style="display: inline-block; text-align: center; margin: 0 auto;">
                                <a href="#" class="btn"><i class="fa fa-eye"></i> View</a>
                            </figure>
                            <img class="card-img img-fluid" src="../assets/images/big/img4.jpg" alt="Card image">
                        </div>
                    </div>`
                ].join("\n");
                var html = Mustache.render(image);
                $("#projectActionViewer").append(html);
            }

            let addImageBtn = document.getElementById('uploadImage');
            addImageBtn.onclick = () => {
                console.log('Adding Picture');
            };
        }
    }
}

export default CurrentProject;