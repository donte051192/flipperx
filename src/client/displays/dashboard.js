'use strict';

import Display from '../Display';
import displayTransitioner from '../display-transitioner';
import Mustache from 'mustache';

class Dashboard extends Display {
    constructor(ws) {
        super('dashboard');
        this.ws = ws;
        this.displayname = 'dashboard';
        this.init();
    }

    init() {
        console.log('Initiating Dashboard');
    }

    open() {
        this.isopen = true;
        return Promise.resolve();
    }

    close() {
        this.isopen = false;
        return Promise.resolve();
    }
}

export default Dashboard;
