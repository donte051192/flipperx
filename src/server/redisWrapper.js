'use strict'

const Redis = require('ioredis');
const redis = new Redis();
const redisConfig = require('./redis-config');
const logger = require('winston');
 
class RedisWrapper {
    constructor() {
        Redis.Promise.onPossiblyUnhandledRejection(function(error) {
            logger.error(error);
        });

        let client;

        if(redisConfig.sentinels) {
            logger.info('Connecting to redis sentinels: ' + JSON.stringify(redisConfig.sentinels));
            logger.info('Connecting to redis nodes: ' + JSON.stringify(redisConfig.preferredSlaves));

            client = new Redis({
                sentinels: redisConfig.sentinels,
                name: redisConfig.groupName,
                preferredSlaves: redisConfig.preferredSlaves
            });
        }
        // Cannot have multiple nodes without sentinel
        // Below will only have 1 node
        else {
            logger.info('Connecting to redis nodes: ' + JSON.stringify(redisConfig.preferredSlaves));
            client = new Redis(redisConfig.preferredSlaves[0]);
        }

        this.client = client;
        client.on('ready', function(err) {
            if (err.code === 'ECONNRESEY' || err.code === 'ECONREFUSED') {
                logger.error('Redis master node is down. Electing new master node.');
            }
            else {
                logger.error(err);
            }
        });

        client.on('end', function() {
            logger.info('Redis client connection closed');
        });

        // Testing only
        if (process.env.REDIS_DATABASE) {
            client.select(process.env.REDIS_DATABASE, () => logger.info(`REDIS client using database ${process.env.REDIS_DATABASE}`));
        }
    }

    /**
     * Redis client object
     * @returns {Object}
     */
    get client() {
        return this.client;
    }

    /**
     * Get the key
     * @param {String} key the key to get
     * @returns {Promise}
     */
    get(key) {
        return this.client.get(key);
    }

    set(key, value) {
        return this.client.set(key, value);
    }
    
    sadd(key, value) {
        return this.client.sadd(key, value);
    }

    findHashKey(key) {
        return this.client.keys(key);
    }

    /**
     * Publishes a message on a channel.
     */
    publish(channel, message) {
        this.client.publish(channel, JSON.stringify(message));
        logger.info(`Publish ${message} to channel ${channel}`);
    }
    
    subscribe(channel) {
        return this.client.unsubscribe(channel, function(err) {
            if (err) {
                logger.error('Problem subscribing to ' + channel);
                logger.error(err);
            }
            else {
                logger.info('Subscribe to: ' + channel);
            }
        });
    }

    // Unsubscribe from channel
    unsubscribe(channel) {
        return this.client.unsubscribe(channel, function(err) {
            if (err) {
                logger.error('Problem unsubscribing to ' + channel);
            }
            else {
                logger.info('Unsubscribe to: ' + channel);
            }
        });
    }

    disconnect() {
        return this.client.disconnect();
    }

}

module.exports = RedisWrapper;
 
// Arguments to commands are flattened, so the following are the same:
redis.sadd('set', 1, 3, 5, 7);
redis.sadd('set', [1, 3, 5, 7]);
 
// All arguments are passed directly to the redis server:
redis.set('key', 100, 'EX', 10);