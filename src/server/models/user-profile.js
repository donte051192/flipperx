let mongoose = require('mongoose');

let profileSchema = mongoose.Schema({

    _id: String, // this is the same id as the user
	name: String,
	email: String,
	timezone: String,
	projects: [
		{
            id: Number
		}
    ],
    paymentinfo: [
		{
            nameoncard: String,
            cardnumber: Number, // needs to be a crytohash
            expiration: String
		}
    ]
});

module.exports = mongoose.model('UserProfile', profileSchema);