const mongoose = require('mongoose');
const bcrypt   = require('bcrypt');
const config = require('../config/dbconfig');
let Schema = mongoose.Schema;

let userSchema = Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        unique: true,
        required: true,
        match: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    },
    password: {
        type: String,
        required: true,
    },
    created_at: Date,
    admin: false
});

userSchema.pre('save', function(next) {
    let currentDate = new Date();
    
    if (!this.created_at) {
        this.created_at = currentDate;
    }
    next();
});

// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);