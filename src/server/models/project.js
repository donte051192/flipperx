let mongoose = require('mongoose');

let projectSchema = mongoose.Schema({

    projectid: mongoose.Schema.Types.ObjectId,
	title: {
		name: String,
		required: true
	},
	collaborators: Array,
	picture: String,
    video: String,
	description: String,
	goal: String,
	startdate: Date,
	enddate: Date,
	contributedamount: String,
	totalcontributors: Number,
	rewards: Object[
		{
            id: Number,
			reward: String,
			eligibility: String,
            
		}
	],
	likes: Number,
	comments: Object[
		{
			id: Number,
			userid: mongoose.Schema.Types.ObjectId,
			comment: String,
			timestamp: Date
		}
	],
	contributorlocations: Array,
	tournamentid: mongoose.Schema.Types.ObjectId
});

module.exports = mongoose.model('Project', projectSchema);