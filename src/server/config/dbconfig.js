// Configuration settings
// Store as environment variables during production

module.exports = {
    'database' : 'mongodb://localhost:27017/flipperX', // the URI with username and password to your MongoDB 
    'secret': 'flipperX' // used to create and verify JSON Web Tokens 
};