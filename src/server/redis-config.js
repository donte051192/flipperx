'use strict'

let redisNodes = '127.0.0.1:6379';
let sentinelNodes = '';
let splitRedisNodes;
let splitSentinelNodes;

let sents;
let preferred;

if (redisNodes) {
    splitRedisNodes = redisNodes.split(',');
    preferred = preferredSlaves();
}
if (sentinelNodes) {
    splitSentinelNodes = sentinelNodes.split(',');
    sent = sentinels();
}

function sentinels() {
    let nodes = [];
    splitSentinelNodes.forEach(node => {
        let hostPortSplit = node.split(':');
        let obj = { host: hostPortSplit[0], port: hostPortSplit[1] };
        nodes.push(obj);
    });
    return nodes;
}

function preferredSlaves() {
    let nodes = [];
    splitRedisNodes.forEach(node => {
        let ipPortPrio = node.split(':');
        let obj = { host: ipPortPrio[0], port: ipPortPrio[1] };
        nodes.push(obj);
    })
};

module.exports = {
    sentinels: sents,
    preferredSlaves: preferred,
    groupName: 'redis- cluster',
    role: 'slave'
}
