'use strict';

const http = require('http');
const express = require('express');
const session = require('express-session');
const app = express();
const server = http.createServer(app);
const bodyParser = require('body-parser');
const io = require('socket.io')(server);
// const socketioJwt = require('socketio-jwt');
const logger = require('winston');
const fs = require('fs');
const mongoose = require('mongoose');
const config = require('./src/server/config/dbconfig');
const flash = require('connect-flash');
const cookieParser = require('cookie-parser');
const DBConnection = mongoose.connection;

let routes = require('./routes');
let currentProject;
// const fetch = require("node-fetch");

//connect to MongoDB
mongoose.connect(config.database);
mongoose.Promise = global.Promise;

DBConnection.once('open', function() {
    logger.info("MongoDB Connected");
});
DBConnection.on('error', function() {
    logger.error('MongoDB Connection Error', arguments);
});
DBConnection.on('reconnected', function () {
    logger.info('MongoDB reconnected!');
});
DBConnection.on('disconnected', function () {
    logger.error('MongoDB Disconnected!');
});

// SETS
app.set('port', process.env.PORT || 3000);

// parse incoming requests
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(session({ 
    secret: config.secret,
    resave: true,
    saveUninitialized: false
})); // session secret
app.use(flash()); // use connect-flash for flash messages stored in session
app.use('/', routes);

// catch 404 and forward to error handler
// app.use(function (req, res, next) {
//     var err = new Error('File Not Found');
//     err.status = 404;
//     next(err);
// });
  
// error handler
// define as the last app.use callback
// app.use(function (err, req, res, next) {
//     res.status(err.status || 500);
//     res.send(err.message);
// });

// const redis = new RedisWrapper();

// Starting the web server
server.listen(app.get('port'), function() {
    logger.info("FlipperX is running on " + app.get('port'));
});

// io.use(socketioJwt.authorize({
//     secret: config.secret,
//     handshake: true
// }));
io.on('connection', function(socket) {
    // console.log('hello!', socket.handshake.decoded_token.name);
    socket.on('error', connectionError);
    registerConnectionCallbacks(socket);
});

/**
 * START ALL SOCKET CONNECTIONS
 * @param {Connection} socket Websocket Connection
 */
function registerConnectionCallbacks(socket) {

    // News
    socket.on('AddProject', data => {
        logger.info('Adding New Project');
        addNewProject(socket, data);
    });

    socket.on('getAllProjects', (data) => {
        getAllProjects(socket);
    });

    socket.on('currentProject', (data) => {
        currentProject =  data;
        socket.emit('currentProject', data);
    });
}

// function redisError(error) {
//     if (error) {
//         logger.error(error.stack);
//     }
// }
/**
 * END ALL SOCKET CONNECTIONS
 */

/**
 * Adds connection to list of tracked connections. Perfoms basic functionality expected
 * of new connections such as sending user id if there is one
 */
function addConnection(connection) {
    connections.add(connection);
}

/**
 * Removes connection from list of tracked connections.
 */
function removeConnection(connection) {
    connections.delete(connection);
}

function connectionError() {
    logger.error('Error in client socket connection');
    logger.error(err.stack || err);
}

function addNewProject(connection, data) {
    // TODO Save to Mongo or Redis
    fs.writeFile(`projectFiles/${data.propertyName}.json`, JSON.stringify(data), (err) => {  
        // throws an error, you could also catch it here
        if (err) throw err;
    
        // success case, the file was saved
        console.log(`New Project ${data.propertyName} saved!`);
    });

    connection.emit('newProjectAdded', data);
}

function getAllProjects(connection) {
    console.log('getting all projs')
    let list = [];
    fs.readdir('./projectFiles', function(err, items) {
        if(err) {
            logger.error('Error in getting projects')
            logger.error(err);
        }
        else {
            items.forEach(item => {
                let promise = new Promise((resolve, reject) => {
                    let path = './projectFiles/'+item;
                    fs.readFile(path, function(err, data) {
                        if (err) {
                            logger.error('Problem reading file: '+ item);
                            reject('Problem Reading File');
                        }
                        else {
                            resolve(JSON.parse(data));
                        }
                    });
                });
                list.push(promise);
            });

            Promise.all(list).then(result => {
                connection.emit('allProjects', result);
            });
        }
    });
}

function getMarketData(connection) {
    let Address = 'http://coincap.io/front';
    makeRequest(Address, connection, 'marketDataReceived');
}

function makeRequest(url, connection, channel) {
    fetch(url).then(response => {
        response.json().then(json => {
            connection.emit(channel, json);
        });
    }).catch(error => {
        console.log(error);
    });
}
