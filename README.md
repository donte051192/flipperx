# Flipper X

A home flipping app that will streamline your real estate business


## Dependencies

1. Node.js version 12.4.0
2. Mongo Database 3.4

## Development Environment

1. Run the powershell script "runDB.ps1" to start the mongo database
2. Download and Install the lastest version of Visual Studio Code (VSCODE)
3. Open this folder in VSCODE
4. Open terminal and type npm install
5. Type "npm start" or click on the debug icon then press start debugging
6. In your browser, navigate to localhost:3000